from django.shortcuts import render,HttpResponse, redirect
from .forms import Jadwal
from .models import Kegiatan 

def home(request):
    return render(request,'home.html')

def product(request):
    return render(request,'product.html')

def resume(request):
    return render(request,'resume.html')

def about(request):
    return render(request,'about.html')

def story(request):
    return render(request,'cerita.html')

def formJadwal(request):
    if(request.method == "POST"):
        form = Jadwal(request.POST)
        if(form.is_valid()):
            form.save()
            return redirect('.')
        else :
            pk = request.POST['id']
            obj = Kegiatan.objects.get(pk=pk)
            obj.delete()
    form = Jadwal()

    list_kegiatan = Kegiatan.objects.all()
    for i in list_kegiatan:
        print(i)
    
    context = {
        'formPavo':form,
        'list_kegiatan': list_kegiatan
    }


    return render(request,'forms.html', context)



    


# def message_post(request):
    
    
#     if(request.method =='POST' and form.is_valid()):
#         hari=request.POST.get("hari")
#         tanggal=request.POST.get("tanggal")
#         jam=request.POST.get("jam")
#         nama_kegiatan=request.POST.get("nama kegiatan")
#         tempat=request.POST.get("tempat")
#         kategori=request.POST.get("kategori")
#         form.save()
#     else:
#         form=Jadwal
        
        
        


