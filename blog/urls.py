from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='blog'

urlpatterns = [
    path('', views.home, name='home'),
    path('product/', views.product, name='product'),
    path('resume/', views.resume, name='resume'),
    path('about/', views.about, name='about'),
    path('product/forms/',views.formJadwal, name='forms'),
    path('product/story/',views.story,name='story'),
]

urlpatterns += staticfiles_urlpatterns()
