from django import forms
from .models import Kegiatan
class Jadwal(forms.ModelForm):
    class Meta:
        model=Kegiatan
        fields= ['hari','tanggal','jam','nama_Kegiatan','tempat','kategori']
    
