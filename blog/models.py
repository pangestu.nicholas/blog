from django.db import models

class Kegiatan(models.Model):
    hari=models.CharField(max_length=20)
    tanggal=models.CharField(max_length=20)
    jam=models.CharField(max_length=10)
    nama_Kegiatan=models.CharField(max_length=200)
    tempat=models.CharField(max_length=100)
    kategori=models.CharField(max_length=60)
    cek=models.DateTimeField()
    
